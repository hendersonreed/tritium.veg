# TRITIUM.VEG --- CONSULT THE ORACLE

[check it out](https://web.cecs.pdx.edu/~hhummel/pages/tritium-veg.html).

It was an unassuming little thing, that old beaten up floppy. Waylon didn't know where it had come from, only that it had ended up in the stack of floppies he hauled around with his IBM PC Jr. The only clue was the loosely scrawled title on the label: **TRITIUM.VEG**

We had met, the first meeting of the sadly-short-lived retrocomputing club, and gathered around the PC Jr - the only piece of hardware at the meeting. Poking around at a few games and playing around with DOS, eventually we had played out all the known floppies.

"I think it might be some kind of demo? Like demoscene." Waylon said, as he popped the other disk out. I handed him the floppy, and we booted the machine, flipping the clunky power switch at the back of the machine.

"Huh, that's weird. I don't remember that." Images had slowly faded on screen, a mess of craggy bitmap fonts, arrayed in a weirdly consistent variety of colors, blinking in and out of existence.

"Must be corrupted."

"Well, it's *really* cool nonetheless." The crew left it there, blinking ceaselessly. 

It was oddly hypnotic.

## Purpose

Much later, I realised how fragile this bizarrely unique and beautiful disk corruption was. The next time I had the opportunity I took a video of the "program" running, and with that in my pocket and a little bit of free time 8 months later, I have emerged victorious! This is a semi-faithful reproduction of the original.

## Caveats

This code/styling is not good or well architected - as a project completed in about 3 hours one evening, with my first attempt at CSS animations, it's barely acceptable. But it's fun to look at.

This also isn't totally faithful to the original. There are a few differences:

* I did not get the exact colors.
* the animation actually fades all the way to black, rather than the character fading to the background color. I could fix this later probably, but I didn't see an immediate solution that didn't require a bunch of ugly hard-coding (as you'll see later, this project has quite enough of that.

## References:

The IBM-inspired font was produced by VileR and can be found here: [The Ultimate Old School Font Pack](https://int10h.org/oldschool-pc-fonts/fontlist/#ibm_pc_bios). It is released under the [CC-BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/).

`chart437.png` is also from them, as full disclosure.
